﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDelegate
{
    public delegate void onCardRevealAllDelegate();
    public static onCardRevealAllDelegate onCardRevealAll;

    public delegate void onCardUnRevealAllDelegate();
    public static onCardUnRevealAllDelegate onCardUnRevealAll;

    public static void OnCardShowAll()
    {
        if (onCardRevealAll != null)
            onCardRevealAll();
    }

    public static void OnCardHideAll()
    {
        if (onCardUnRevealAll != null)
            onCardUnRevealAll();
    }
}
