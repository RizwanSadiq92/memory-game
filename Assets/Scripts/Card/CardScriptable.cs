﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " CardType", menuName = "Card")]
public class CardScriptable : ScriptableObject
{
    public string objectName;

    public string description;

    public Texture2D art;

    public int cardNo;
    
}
