﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{

    public CardScriptable card;

    public Image cardFront;

    public Text cardName;

    public int cardNo;


    private void Start()
    {
        AssignCardValues();
    }

    void AssignCardValues()
    {

        cardFront.sprite = Sprite.Create(card.art, new Rect(0, 0, 480, 800), new Vector2(0.5f, 0.3f));
        cardName.text = card.objectName;
        cardNo = card.cardNo;

    }
}
