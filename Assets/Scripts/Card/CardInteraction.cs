﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInteraction : MonoBehaviour, ICardReveal
{

    #region Data

    private Animator cardAnimator;//this card Animation
    private GameController gameController;// main controller
    private bool revealComplete;// to verify that card has done flipping

    #endregion Data

    #region Unity Callbacks
    void OnEnable()
    {
        CardDelegate.onCardRevealAll += CardReveal;
        CardDelegate.onCardUnRevealAll += CardUnReveal;
    }

    void OnDisable()
    {
        CardDelegate.onCardRevealAll -= CardReveal;
        CardDelegate.onCardUnRevealAll -= CardUnReveal;
    }


    void Awake()
    {
        cardAnimator = GetComponent<Animator>();
        gameController = FindObjectOfType<GameController>();
    }

    #endregion Unity Callbacks

    #region Card CallEvents

    //Calls when player click on a card
    public void CardReveal()
    {
        if (!gameController.canReveal || revealComplete)
            return;

        cardAnimator.enabled = true;
        cardAnimator.SetTrigger("CardReveal"); //Play Animation
        revealComplete = true;

        
    }
    // we can call this function whenever we want to hide the card
    public void CardUnReveal()
    {
        cardAnimator.enabled = true;
        cardAnimator.SetTrigger("CardUnReveal");
        revealComplete = false;

    }

    #endregion Card CallEvents


    #region Animator Events

    // this are called from the animator when ever a card is flipped
    public void SetCardValuesFront()
    {
        cardAnimator.StopPlayback();
        cardAnimator.enabled = false;
        gameController.CardRevealed(this.GetComponent<Card>());// this will call the controller and controller will check the match
    }

    // this are called from the animator when ever a card is flipped
    public void SetCardValuesBack()
    {
        cardAnimator.StopPlayback();
        cardAnimator.enabled = false;
    }

    #endregion Animator Events
}
