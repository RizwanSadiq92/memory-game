﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICardReveal 
{
    void CardReveal();

    void CardUnReveal();
}
