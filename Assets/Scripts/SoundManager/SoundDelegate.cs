﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDelegate : MonoBehaviour
{

    public delegate void ButtonClickDelegate();
    public static event ButtonClickDelegate onButtonClick;

    public delegate void GameCompleteDelegate();
    public static event GameCompleteDelegate onGameComplete;

    public delegate void CardFoundDelegate();
    public static event CardFoundDelegate onCardFound;


    public static void OnButtonClick()
    {
        if(onButtonClick!= null)
        {
            onButtonClick.Invoke();
        }
    }

    public static void OnGameComplete()
    {
        if (onGameComplete != null)
        {
            onGameComplete.Invoke();
        }
    }

    public static void OnCardFound()
    {
        if (onCardFound != null)
        {
            onCardFound.Invoke();
        }
    }

}
