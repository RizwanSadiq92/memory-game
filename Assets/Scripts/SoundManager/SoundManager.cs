﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    #region Data

    static SoundManager _instance;

    #region Private Field

    #region Audio Source

    [SerializeField]
    private AudioSource _music;

    [SerializeField]
    private AudioSource _sfx;

    #endregion Audio Source


    #region Audio Clip

    [SerializeField]
    private AudioClip buttonClickEffect;

    [SerializeField]
    private AudioClip mainMusicEffect;

    [SerializeField]
    private AudioClip cardFoundEffect;

    [SerializeField]
    private AudioClip gameCompleteEffect;


    #endregion Audio Clip


    #endregion Private Field

    #endregion Data

    #region Unity Callbacks

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        OnMusicPlay();
    }

    private void OnEnable()
    {
        SoundDelegate.onButtonClick += OnButtonClick;
        SoundDelegate.onCardFound += OnCardFound;
        SoundDelegate.onGameComplete += OnGameComplete;
    }

    private void OnDisable()
    {
        SoundDelegate.onButtonClick -= OnButtonClick;
        SoundDelegate.onCardFound -= OnCardFound;
        SoundDelegate.onGameComplete -= OnGameComplete;
    }

    #endregion Unity Callbacks

    #region Sound CallBacks

    public void OnButtonClick()
    {
        _sfx.PlayOneShot(buttonClickEffect);
    }

    public void OnMusicPlay()
    {
        _music.clip = mainMusicEffect;
        _music.Play();
    }

    public void OnCardFound()
    {
        _sfx.PlayOneShot(cardFoundEffect);
    }

    public void OnGameComplete()
    {
        _sfx.PlayOneShot(gameCompleteEffect);
    }

    #endregion Sound CallBacks
}
