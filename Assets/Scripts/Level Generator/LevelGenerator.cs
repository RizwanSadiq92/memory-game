﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class RowsCols
{
    public int rows = 2;
    public int cols = 2;
    public int totalMoves=5;
}
[RequireComponent(typeof(CardGenerator))]
public class LevelGenerator : MonoBehaviour
{

    #region Data


    #region Private Field

    [SerializeField]
    private int rows;

    [SerializeField]
    private int cols;

    [SerializeField]
    private GameObject referencePoint;

    [SerializeField]
    private float cardSpaces;

    private List<Transform> cardPos;

    private CardGenerator cardGenerator;

    #endregion Private Field


    #region Public Field

    public GameObject cam;

    #endregion Public Field

    #endregion Data


    private void Awake()
    {
        cardPos = new List<Transform>();
        referencePoint.transform.localScale = Vector3.one;
        cardGenerator = GetComponent<CardGenerator>();
    }

    public  void SetValuesGrid(int _row,int _col)
    {

        rows = _row;
        cols = _col;

        GenerateGrid();
        AdjustCameraView();
        
    }


    #region Grid Generator
    // this can show all the cards at first then hide it
    IEnumerator DisplayCard()
    {
        yield return new WaitForSeconds(1f);

        CardDelegate.OnCardShowAll();

        yield return new WaitForSeconds(3f);

        CardDelegate.OnCardHideAll();

    }
    
    
    // this function will generate a grid with respect rows and cols and will call card Generator and send all card temp Positions
    void GenerateGrid()
    {

        for (int col = 0; col < cols; col++)
        {

            for (int row = 0; row < rows; row++)
            {

                GameObject gridTemp = (GameObject)Instantiate(referencePoint, transform);

                float posX = col * cardSpaces;
                float posY = row * -(cardSpaces+3);
                gridTemp.transform.position = new Vector2(posX,posY);
                cardPos.Add(gridTemp.transform);

            }
        }
        cardGenerator.AddCards(cardPos);
        Destroy(referencePoint);
       
    }

    public Vector3 offset;// to give a gap between top and left

    void AdjustCameraView()
    {


        // Finding the midpoint from first card to last card
        Vector3 midPoint = new Vector3((cardPos[cardPos.Count - 1].position.x - cardPos[0].position.x)/2, (cardPos[cardPos.Count - 1].position.y - cardPos[0].position.y)/2,
            (cardPos[cardPos.Count - 1].position.z - cardPos[0].position.z)/2);

      
        cam.transform.position = new Vector3(midPoint.x,midPoint.y,cam.transform.position.z); // Camera always will move towards the midpoint between the all the cards

        Vector3 viewPos = Camera.main.WorldToScreenPoint(cardPos[0].transform.position + offset); // to check that all cards are in view or not

        float tempZoom = cam.transform.position.z;

        // Camera Zoom out with respect to z axis so that every card is in view

        // For Width Check
        while (viewPos.x <= 0)
        {
            tempZoom = tempZoom - 10;
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, tempZoom);
            viewPos = Camera.main.WorldToScreenPoint(cardPos[0].transform.position + offset);
            //Debug.Log(" Screen " + Screen.width + "Pos" + viewPos);
        }



        // For Height Check

        while (viewPos.y > (Screen.height -250f))
        {
            tempZoom = tempZoom - 10;
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, tempZoom);
            viewPos = Camera.main.WorldToScreenPoint(cardPos[0].transform.position + offset);
            //Debug.Log(" Screen " + Screen.width + "Pos" + viewPos);
        }

    }

    #endregion Grid Generator

}
