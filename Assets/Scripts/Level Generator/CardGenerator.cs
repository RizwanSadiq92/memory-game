﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGenerator : MonoBehaviour
{
    #region Data


    #region Private Field


    private int cardTypeNo = 0;

    private bool changeType;

    private int totalPairs;

    [SerializeField]
    private List<Transform> cardPos;

    [SerializeField]
    private GameObject cardPrefab;


    #endregion Private Field


    #region Public Field

    public List<CardScriptable> cardType;

    #endregion Public Field


    #endregion Data


    #region Unity CallBacks

    private void Start()
    {
        cardPos = new List<Transform>();
    }


    #endregion Unity CallBacks


    #region Asssign Cards

    public void AddCards(List<Transform> tempPos)
    {
        //getting values from levelGenerator
        foreach (var item in tempPos)
        {
            cardPos.Add(item);
        }

        

        GameController.totalPairs = cardPos.Count / 2; // Assigning values to main controller

        totalPairs = cardPos.Count / 2;

        while (cardPos.Count != 0)
        {
            CreateCard(Random.Range(0, cardPos.Count)); // random function will always choose randomness of each card position
        }

    }

    // Create Card and assign to the positions recieve from the level Generator
    void CreateCard(int tempNo)
    {
        // this will make sure that card Should be generated in pairs
        if (totalPairs <= 0 )
        {
            cardPos.RemoveAt(tempNo);
            return;

        }

        GameObject tempCard = (GameObject)Instantiate(cardPrefab, transform); // Creating new card from prefab

        tempCard.GetComponent<Card>().card = cardType[cardTypeNo]; // Assigning Values and card type


        // this will help to identify that Type is applied in pairs
        if (changeType)
        {
            cardTypeNo++;
            changeType = false;
            totalPairs--;
        }
        else
        {
            changeType = true;
        }

        
        tempCard.transform.position = cardPos[tempNo].position;

        cardPos.RemoveAt(tempNo);
    }

    #endregion Asssign Cards
}
