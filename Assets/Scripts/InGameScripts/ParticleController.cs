﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public GameObject _starParticle;


    #region Unity Callbacks
    private void OnEnable()
    {
        GameEventDelegate.onCardFound += ShowFoundEffect;
    }

    private void OnDisable()
    {
        GameEventDelegate.onCardFound -= ShowFoundEffect;
    }

    #endregion Unity Callbacks



    public void ShowFoundEffect()
    {
        GameObject _tempEffect = Instantiate(_starParticle);
        _tempEffect.transform.position = Camera.main.transform.position + new Vector3(0, 0, 10);
    }
}
