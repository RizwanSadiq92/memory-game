﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventDelegate 
{
    
    public delegate void onCardRevealAllDelegate();
    public static onCardRevealAllDelegate onCardFound;


    public delegate void onFetchingDataDelegate();
    public static  onFetchingDataDelegate onCardnFetchingData;

    public delegate void onFetchDataLoadDelegate();
    public static onFetchDataLoadDelegate onLoadingData;

    public delegate void oonGameCompleteSuccessDelegate();
    public static oonGameCompleteSuccessDelegate onGameCompleteSuccess;

    public delegate void onGameCompleteFailDelegate();
    public static onGameCompleteFailDelegate onGameCompleteFail;

    public static void CardFound()
    {
        if (onCardFound != null)
        {
            onCardFound.Invoke();
        }
    }

    public static void FetchedDataComplete()
    {
        if (onCardnFetchingData != null)
        {
            onCardnFetchingData.Invoke();
        }
    }

    public static void LoadingData()
    {
        if (onLoadingData != null)
        {
            onLoadingData.Invoke();
        }
    }

    public static void GameCompleteSuccess()
    {
        if (onGameCompleteSuccess != null)
        {
            onGameCompleteSuccess.Invoke();
        }
    }

    public static void GameCompleteFail()
    {
        if (onGameCompleteFail != null)
        {
            onGameCompleteFail.Invoke();
        }
    }
}
