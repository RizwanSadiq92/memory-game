﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    #region Data


    #region Private Fields
    private Card _firstCardRevealed;
    private Card _secondCardRevealed;
    [SerializeField]
    private int moves = 10;
    List<Card> listOfCards;

    #endregion Private Fields


    #region Public Fields

    public static int totalPairs;

    public int LevelNo;

    [SerializeField]
    public RowsCols[] rowsCols;

    #endregion Public Fields

    LevelGenerator levelGenerator;
    UIController uIController;

    
    #endregion Data


    private void Start()
    {

        AsssignReferences();
        CheckLevelAndSet(LevelNo);
        
        
    }

    // set the rows and cols with respect to level No
    void CheckLevelAndSet(int _levelNo)
    {
        levelGenerator.SetValuesGrid(rowsCols[_levelNo - 1].rows, rowsCols[_levelNo - 1].cols);
        moves = rowsCols[_levelNo - 1].totalMoves;
    }


    void AsssignReferences()
    {
        LevelNo = PlayerPrefs.GetInt("LevelNo",0);
        listOfCards = new List<Card>();
        levelGenerator = FindObjectOfType<LevelGenerator>();
        uIController = FindObjectOfType<UIController>();
    }


    #region Card Match

    public bool canReveal
    {
        get { return _secondCardRevealed == null; }
    }


    //this is called when ever a card is revealed and will send its value
    public void CardRevealed(Card card)
    {
        
        if(_firstCardRevealed == null)
        {
            _firstCardRevealed = card;
            listOfCards.Add(card);
        }
        else
        {
            _secondCardRevealed = card;
            listOfCards.Add(card);
            StartCoroutine("CheckMatch");
            moves--;
            uIController.SetMoves(moves);


        }
    }

    //this function will compare the two cards and assign the game state
    private IEnumerator CheckMatch()
    {
        if(_firstCardRevealed.cardNo == _secondCardRevealed.cardNo)
        {
            
            listOfCards.Clear();
            totalPairs--;
            GameEventDelegate.onCardFound();
            SoundDelegate.OnCardFound();
        }
        else
        {
            yield return new WaitForSeconds(0.5f);

            for(int i=0;i < listOfCards.Count;i++)
            {
                listOfCards[i].GetComponent<ICardReveal>().CardUnReveal();
            }
            listOfCards.Clear();
        }

        if (moves == 0 && totalPairs !=0)
        {
            GameEventDelegate.GameCompleteFail();
            Debug.Log("Game Over");
        }else if(totalPairs== 0)
        {
            GameEventDelegate.GameCompleteSuccess();
            Debug.Log("Game Win");
        }

        _firstCardRevealed = null;
        _secondCardRevealed = null;
    }

    #endregion Card Match
}
