﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region Data

    public Text _movesText;


    #region Buttons
    [SerializeField]
    private Button _homeButton;

    [SerializeField]
    private Button _resetButton;



    #endregion Buttons


    #region Panels

    [SerializeField]
    private GameObject _levelCompletePanel;
    [SerializeField]
    private GameObject _levelFailPanel;

    #endregion Panels



    #endregion Data

    private void OnEnable()
    {
        SetDelegateReferences();
        SetButtonReferences();
    }

    private void OnDisable()
    {
        GameEventDelegate.onGameCompleteSuccess -= LevelComplete;
        GameEventDelegate.onGameCompleteFail -= LevelFail;
    }

    #region References
    void SetDelegateReferences()
    {
        GameEventDelegate.onGameCompleteSuccess += LevelComplete;
        GameEventDelegate.onGameCompleteFail += LevelFail;
    }

    void SetButtonReferences()
    {
        _homeButton.onClick.AddListener(delegate { HomeOnClick(); });
        _resetButton.onClick.AddListener(delegate { RestartOnClick(); });
    }

    #endregion References

    public void SetMoves(int value)
    {
        _movesText.text = value.ToString() ;
    }




    #region Button CallBacks

    public void HomeOnClick()
    {
        SceneManager.LoadScene("Menu");
    }

    public void RestartOnClick()
    {
        SceneManager.LoadScene("Game");
    }


    #endregion Button CallBacks


    #region Game State


    private void LevelComplete()
    {
        _levelCompletePanel.SetActive(true);
        SoundDelegate.OnGameComplete();
    }

    private void LevelFail()
    {
        _levelFailPanel.SetActive(true);
        SoundDelegate.OnGameComplete();
    }


    #endregion Game State
}
