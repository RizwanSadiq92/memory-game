﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class SaveLoadData : MonoBehaviour
{


    #region Data

    #region Private Field

    static bool DataFetched = false;
    [SerializeField] private PlayerData playerData = new PlayerData();

    #endregion Private Field

    #region Public Field

    public CardScriptable[] cardContainer;

    public static string savePath;
    public static string path;

    #endregion Public Field

    #endregion Data



    void Awake()
    {

        savePath = Application.persistentDataPath + "/";

        path = Path.Combine(savePath, "CardData.json");
        PlayerPrefs.SetString("GameData", path);

        Debug.Log(""+ path);
    }

    private void Start()
    {
        if(!DataFetched)
            LoadData();
        else
        {
            GameEventDelegate.FetchedDataComplete();
        }
    }


    public  void SaveData(PlayerData playerData2)
    {
        Debug.Log("inhere");
        string cardData = JsonUtility.ToJson(playerData2);
        
        File.WriteAllText(path, cardData);

#if UNITY_EDITOR
        SceneManager.LoadScene(0);
#elif UNITY_ANDROID
        LoadData();
#endif

    }


    // this will first try to load the data if it contains value
    // otherwise it will fetch from server
    public  void LoadData()
    {
        Debug.Log("In  here");

        try
        {
            string data = File.ReadAllText(path);

            PlayerData tempData = JsonUtility.FromJson<PlayerData>(data);
            StartCoroutine(SetDataValues(tempData));
        }
        catch (Exception e)
        {
            GameObject.FindObjectOfType<DataFetch>().GetData(); // this will get the data from server
        }

    }

    //Setting texture from html into scriptable objects

    IEnumerator SetDataValues(PlayerData playerData)
    {
        for (int i = 0; i < cardContainer.Length; i++)
        {
            cardContainer[i].objectName = playerData.cardTypes[i].tags[0];
            Debug.Log("name "  + cardContainer[i].name);
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(playerData.cardTypes[i].url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                cardContainer[i].art = (Texture2D)myTexture;
            }
            GameEventDelegate.LoadingData();
        }

        GameEventDelegate.FetchedDataComplete();
        DataFetched = true;
    }


}