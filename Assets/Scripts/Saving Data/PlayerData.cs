﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerData
{
    public string name;
    public int highscore;

    public List<CardTypeData> cardTypes = new List<CardTypeData>();
}

[System.Serializable]
public class CardTypeData
{
    public string[] tags;
    public string url = "";

}
