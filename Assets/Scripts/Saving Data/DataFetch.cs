using System.Collections;
using System.IO;

using RestSharp;
using UnityEngine;


public class DataFetch : MonoBehaviour
{

    public string[] tags;

    [SerializeField]
    int tempDataNo=0;

    private PlayerData playerData = new PlayerData();
    public SaveLoadData sd;

    

    private void Start()
    {
        sd = GetComponent<SaveLoadData>();

    }

    public void GetData()
    {
        
        var client = new RestClient("https://mlemapi.p.rapidapi.com/randommlem?tag="+tags[tempDataNo]+"&orientation=portrait");

        var request = new RestRequest(Method.GET);
        request.AddHeader("x-rapidapi-host", "mlemapi.p.rapidapi.com");
        request.AddHeader("x-rapidapi-key", "3323fe3bf2msh0799342bc1b1c5ap1a9b9ejsnfb8387b2da3a");


        client.ExecuteAsync(request, (response) =>
        {
            callback(response.Content);
        });
    }

    private void callback(string content)
    {

        

        if (tempDataNo < tags.Length)
        {
            CardTypeData tempData = JsonUtility.FromJson<CardTypeData>(content);

            playerData.cardTypes.Add(tempData);
            //Debug.Log("In "+ tempDataNo);
            tempDataNo++;
            GetData();

        }
        else
        {
            sd.SaveData(playerData);
        }
    }

}
