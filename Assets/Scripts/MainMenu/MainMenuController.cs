﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{

    #region UI Buttons
    [SerializeField]
    private Button _questButton;
    [SerializeField]
    private Button[] _backButton;
    [SerializeField]
    private Button _friendButton;

    #endregion UI Buttons

    #region UI Panels

    [SerializeField]
    private GameObject LevelPanel;
    [SerializeField]
    private GameObject MenuPanel;
    [SerializeField]
    private GameObject LoadingPanel;
    [SerializeField]
    private GameObject FriendPanel;

    #endregion UI Panels


    public Image _innerBar;
    public Text loadingText;


    private void OnEnable()
    {
        _questButton.onClick.AddListener(delegate { OnQuest();});

        foreach(var item in _backButton)
            item.onClick.AddListener(delegate { OnBackButton(); });

        _friendButton.onClick.AddListener(delegate { OpenFriend(); });

        GameEventDelegate.onCardnFetchingData += DataLoadingComplete;
        GameEventDelegate.onLoadingData += dataLoadingBar;


    }

    private void OnDisable()
    {
        GameEventDelegate.onCardnFetchingData -= DataLoadingComplete;
        GameEventDelegate.onLoadingData -= dataLoadingBar;
    }



    public void OnQuest()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(true);

        SoundDelegate.OnButtonClick();
    }

    public void OpenFriend()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(false);
        FriendPanel.SetActive(true);
        SoundDelegate.OnButtonClick();
    }

    public void OnBackButton()
    {
        MenuPanel.SetActive(true);
        LevelPanel.SetActive(false);
        FriendPanel.SetActive(false);
        SoundDelegate.OnButtonClick();
    }

    public void OpenLevel(int levelNo)
    {
        PlayerPrefs.SetInt("LevelNo", levelNo);
        SoundDelegate.OnButtonClick();
        OpenScene("Game");
    }


    public void OpenScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    #region Gethering Data


    public void dataLoadingBar()
    {
        loadingText.text = "Loading from Memory";
        _innerBar.fillAmount += 0.1f;
       
    }

    public void DataLoadingComplete()
    {
        LoadingPanel.SetActive(false);
        MenuPanel.SetActive(true);
    }

    

    #endregion Gethering Data

}
