﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendList : MonoBehaviour
{
    public Transform parent;
    public GameObject Friend;
    public CardScriptable[] cardContainer;

    private void OnEnable()
    {
        GameEventDelegate.onCardnFetchingData += AssignFriendValues;
    }
    private void OnDisable()
    {
        GameEventDelegate.onCardnFetchingData -= AssignFriendValues;
    }

    public void AssignFriendValues()
    {
        for (int i=0 ; i< cardContainer.Length ; i++)
        {
            GameObject tempFriend = Instantiate(Friend, parent);
            tempFriend.GetComponent<FriendContainer>().SetValues(Sprite.Create(cardContainer[i].art, new Rect(0, 0, 480, 800), new Vector2(0.5f, 0.3f)), cardContainer[i].objectName);
        }
    }
}
