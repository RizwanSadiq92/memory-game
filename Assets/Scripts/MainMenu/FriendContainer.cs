﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendContainer : MonoBehaviour
{
    [SerializeField]
    private Image spriteImage;
    [SerializeField]
    private Text name;

    public void SetValues(Sprite tempPic, string tempName)
    {
        spriteImage.sprite = tempPic;
        name.text = tempName;
    }

}
